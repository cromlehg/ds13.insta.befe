name := """ds13.insta.befe"""
organization := "ds13"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies += filters
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test

libraryDependencies ++= Seq(
  cache,
  ws,
  "ds13" % "logger_2.11" % "0.1" changing(), // It needs to update local ivy cache (not for rerleases)
  "ds13" % "bots_2.11" % "0.1" changing(), // It needs to update local ivy cache (not for releases)
  "ds13" % "http_2.11" % "0.1" changing(),
  "ds13" % "insta_2.11" % "0.1" changing(),

  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "mysql" % "mysql-connector-java" % "6.0.5",
  "com.typesafe.play" %% "play-slick" % "2.0.0",

  "com.github.nscala-time" %% "nscala-time" % "2.12.0",

  "org.webjars" % "jquery" % "2.2.1",
  "org.webjars" % "bootstrap" % "3.3.6" exclude("org.webjars", "jquery"),

  "org.webjars" % "x-editable-bootstrap3" % "1.5.1-1",

  "com.github.t3hnar" %% "scala-bcrypt" % "2.6",
  "org.webjars" % "respond" % "1.4.2",
  "org.webjars" % "html5shiv" % "3.7.3",
  "org.webjars" % "font-awesome" % "4.6.1",
  "org.webjars" % "ionicons" % "2.0.1",
  "org.webjars" % "dropzone" % "4.2.0",
  "org.webjars.bower" % "iCheck" % "1.0.2",
  
  "jp.t2v" %% "play2-auth"        % "0.14.2"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

includeFilter in (Assets, LessKeys.less) := "*.less"
scalacOptions += "-feature"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "ds13.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "ds13.binders._"
