/*
 * for user: test@test.test, test
 * 
 */
INSERT INTO user_roles VALUES(1, "admin");

INSERT INTO user_roles VALUES(2, "client");

INSERT INTO users VALUES(1, "iceberg", "cromlehg@gmail.com", "$2a$10$O9lRgNf9yxBvUTGUsA918.CcZBRygZZYM59GEskSV06A8VN7vXa3W", null, 1, 1, "Alexander", "Strakh", 1, 1463047451);

INSERT INTO users VALUES(2, "partizan", "lm333@mail.ru", "$2a$10$O9lRgNf9yxBtUTGUsA918.CcZBRygZZYM59GEskSV06A8VN7vXa3W", null, 1, 1, "Michael", "Lozovikov", 2, 1463047451);

INSERT INTO users VALUES(3, "apetchaninov", "petchaninov@gmail.com", "$2a$10$O2lRgNf7yxBtUTGUsA918.CcZBRygZZZM59GEskSV06A8VN7vXa3W", null, 1, 1, "Anton", "Petchaninov", 2, 1463047451);


