import com.google.inject.AbstractModule

import controllers.modules.robots.IBEFEBotContoller
import controllers.modules.robots.RobotsProvider
import controllers.modules.robots.impl.inst.FollowBot1Descriptor
import controllers.modules.robots.impl.inst.UnfollowBot1Descriptor
import controllers.modules.robots.impl.inst.UserFollowingsFollowBot1Descriptor
import javax.inject.Provider
import javax.inject.Singleton
import controllers.modules.robots.impl.inst.ComplexBot1Descriptor

class Module extends AbstractModule {

  override def configure() = {

    bind(classOf[RobotsProvider]).toProvider(new Provider[RobotsProvider]() {
      override def get = new RobotsProvider(
        new UnfollowBot1Descriptor,
        new FollowBot1Descriptor,
        new UserFollowingsFollowBot1Descriptor,
        new ComplexBot1Descriptor)
    }).in(classOf[Singleton])

    bind(classOf[IBEFEBotContoller]).in(classOf[Singleton])

  }

}
