package controllers

import jp.t2v.lab.play2.auth.AsyncAuth
import jp.t2v.lab.play2.auth.LoginLogout
import jp.t2v.lab.play2.auth.AuthElement

import jp.t2v.lab.play2.auth.AuthActionBuilders

import javax.inject._
import play.api._
import play.api.mvc._

import scala.concurrent.ExecutionContext
import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import play.api.data._
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText

import models.daos.DAO
import scala.util.Random

import org.mindrot.jbcrypt.BCrypt
import controllers.security.AuthConfigImpl
import play.mvc.Http.RequestHeader
import models.User
import javax.security.auth.Subject

import models.Roles

trait TraitDS13UserServiceWithRoles extends TraitUserServiceWithRoles {

//  def actionBuilder(ownerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) = new ActionBuilder[Request] {
//    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
//
//      //      dao.findUserById(ownerId) map {
//      //        case Some(ownerUser) => ownerOrAdminActionAsyncRequestUser(ownerUser.id)(f(ownerUser))
//      //        case _               => BadRequest("Couldn't get user with id " + ownerId + "!")
//      //      }
//
//      Logger.info("Calling action")
//      block(request)
//    }
//  }
//
//  def composedActions(ownerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
//    actionBuilder(ownerId)(f) { request =>
//      Ok("")
//    }

  //  def ownerOrAdmin1(ownerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
  //    ownerOrAdminActionAsyncRequestUser(ownerId)(f)

  //  def ownerOrAdmin(ownerId: Long)(f: User => (User => (AuthRequest[AnyContent] => Future[Result])))(implicit context: ExecutionContext) =
  //    Action {
  //      dao.findUserById(ownerId) map {
  //        case Some(ownerUser) => ownerOrAdminActionAsyncRequestUser(ownerUser.id)(f(ownerUser))
  //        case _               => BadRequest("Couldn't get user with id " + ownerId + "!")
  //      }
  //    }
  //
  //  def accountOwnerOrAdmin(accountId: Long)(f: (Long, Long, String, String, Option[Long]) => (User => (User => (AuthRequest[AnyContent] => Future[Result]))))(implicit context: ExecutionContext) =
  //    dao.findAccountById(accountId) flatMap {
  //      case Some(account) => ownerOrAdmin(account._2)(f tupled account)
  //      case _             => Future(Action(BadRequest("Couldn't get account with id " + accountId + "!")))
  //    }
  //
  //  def apiOwnerOrAdmin(apiId: Long)(f: => (Long, Long, String, String, Option[String], Option[String], Option[Long]) => ((Long, Long, String, String, Option[Long]) => (User => (User => (AuthRequest[AnyContent] => Future[Result])))))(implicit context: ExecutionContext) =
  //    dao.findAPIById(apiId) flatMap {
  //      case Some(api) => accountOwnerOrAdmin(api._2)(f tupled api)
  //      case _         => Future(Action(BadRequest("Couldn't get API with id " + apiId + "!")))
  //    }

}
