package controllers.modules.robots

import javax.inject.Singleton

@Singleton
class RobotsProvider(val descriptors: IBEFEBotDescriptor*) 