package controllers.modules.robots

import ds13.logger.Logger

object LogStorageLogger {

  def apply(logStorage: LogStorage) =
    new LogStorageLogger(logStorage)

}

class LogStorageLogger(logStorage: LogStorage) extends Logger {

  override def log(text: String, severity: Int) =
    logStorage.append(text)

}