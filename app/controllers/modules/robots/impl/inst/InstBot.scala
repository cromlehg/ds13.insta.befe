package controllers.modules.robots.impl.inst

import controllers.modules.robots.IBEFEBotProcess
import ds13.insta.impl.rc1.APIExecutor

class InstBot(logger: ds13.logger.Logger, id: Long, name: String, api: APIExecutor)
    extends IBEFEBotProcess(logger, id, api, name) {

}
