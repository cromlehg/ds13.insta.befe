package controllers.modules.robots.impl.inst

import controllers.modules.robots.IBEFEBotConfig
import controllers.modules.robots.IBEFEBotDescriptor
import controllers.modules.robots.IBEFEBotHelper
import controllers.modules.robots.IBEFEBotProcess
import controllers.modules.robots.LogStorage
import javax.inject.Singleton
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.i18n.Messages
import play.api.mvc.Flash
import controllers.modules.robots.LogStorageLogger
import ds13.insta.impl.rc1.APIExecutor
import scala.collection.mutable.ListBuffer
import models.AccountAPI
import ds13.insta.impl.rc1.SPStates

case class UnfollowBot1FormConfig(val phrase: String)

class UnfollowBot1Descriptor extends IBEFEBotDescriptor("Unfollow robot v1") {

  val PHRASE = "phrase"

  override def createPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.unfollowBot1ConfigPage(ownerUser, apiId, configForm, this)
    
  override def infoPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: play.api.mvc.Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.unfollowBot1InfoPage(ownerUser, apiId, configForm, this)        

  override def configForm: Form[UnfollowBot1FormConfig] = Form(
    mapping(PHRASE -> nonEmptyText)(UnfollowBot1FormConfig.apply)(UnfollowBot1FormConfig.unapply))

  override def descr: String = name

  override def updateConfig(configData: Any, config: IBEFEBotConfig) =
    configData match {
      case c: UnfollowBot1FormConfig =>
        config + (PHRASE, c.phrase)
        true
      case _ => false
    }

  override def createHelper(config: IBEFEBotConfig): Option[IBEFEBotHelper] = {
    Some(new IBEFEBotHelper(LogStorageLogger(config get[LogStorage] LogStorage.LOG), config, name) {
      override def createBotProcess(config: IBEFEBotConfig, id: Long): Option[IBEFEBotProcess] = {
        val accountInfo = config get[AccountAPI] "account"
        val api = new APIExecutor(logger, accountInfo.userName, accountInfo.instaLogin)
        Some(new UnfollowBot1(logger, id, name, api))
      }
    })
  }

}

class UnfollowBot1(logger: ds13.logger.Logger, id: Long, name: String, api: APIExecutor) extends InstBot(logger, id, name, api) {
  
  state = SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL

  var nextFollwoingsMaxId: Option[String] = None

  val usersPool = ListBuffer[Long]()

  val preparedUsers = ListBuffer[Long]()

  override def catchedStep: Unit =
    state match {
      case SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL =>
        api.executeInternal { internalAPI =>
          val r = internalAPI.getSelfFollowings(nextFollwoingsMaxId)
          nextFollwoingsMaxId = r.nextMaxIdOpt
          usersPool ++= r.users.map(_.id)
          debug("Unfollow users pool filled, now it have size: " + usersPool.length)
          setState(SPStates.STATE_NEEDS_UNFOLLOW)
        }
      case SPStates.STATE_NEEDS_UNFOLLOW =>
        if (usersPool.isEmpty) {
          nextFollwoingsMaxId match {
            case Some(nextMaxId) =>
              debug("Followings pool is empty. Going to pool fill")
              setState(SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL)
            case _ =>
              debug("Unfollowing process finished!")
              setState(SPStates.STATE_FINISHED)
              isFinishedFlag = true
          }
        } else
          api.executeInternal { internalAPI =>
            val userToUnfollow = usersPool.head
            debug("Try to unfollow user with Id: " + userToUnfollow)
            internalAPI.unfollow(userToUnfollow)
            usersPool -= userToUnfollow
            preparedUsers += userToUnfollow
            debug("User with id = " + userToUnfollow + " unfollowed successfully")
            debug("Summay unfollowed: " + preparedUsers.length)
          }
      case _ =>
    }

  override def getPause =
    state match {
      case SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL => 1000
      case SPStates.STATE_NEEDS_UNFOLLOW             => 40000
      case SPStates.STATE_FINISHED                   => 1000
      case _                                         => 60000
    }

}

object ConfigOpts {
  
  val USERNAME = "username"
  
  val ACCOUNTNAME = "accountname"
  
}
