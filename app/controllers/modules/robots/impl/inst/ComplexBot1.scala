package controllers.modules.robots.impl.inst

import controllers.modules.robots.IBEFEBotConfig
import controllers.modules.robots.IBEFEBotDescriptor
import controllers.modules.robots.IBEFEBotHelper
import controllers.modules.robots.IBEFEBotProcess
import controllers.modules.robots.LogStorage
import javax.inject.Singleton
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.default
import play.api.data.Forms.longNumber
import play.api.data.Forms.number
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.boolean
import play.api.i18n.Messages
import play.api.mvc.Flash
import controllers.modules.robots.LogStorageLogger
import ds13.insta.impl.rc1.APIExecutor
import scala.collection.mutable.ListBuffer
import models.AccountAPI
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import ds13.insta.api.intrnl.model.User
import org.json.JSONObject
import play.api.data.validation.Constraint
import play.api.data.validation.ValidationError
import play.api.data.validation.Valid
import play.api.data.validation.Invalid
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.insta.api.intrnl.model.UsersList
import ds13.insta.storage.TraitNotToFollowListStorage
import ds13.insta.storage.TraitNotToUnFollowListStorage
import ds13.insta.impl.rc1.SPStates
import ds13.insta.api.BadGatewayException
import scala.annotation.tailrec
import ds13.insta.api.ClientException
import ds13.insta.api.NotAuthorizedToViewUser
import ds13.insta.api.intrnl.model.providers.TraitFilter

case class ComplexBot1FormConfig(
    val lesfi: String,
    val isNeedToFillSelfFollowersAsNonToUnfollowAndNonToFollow: Boolean,
    val isNeedToFillSelfFollowingsAsNonToFollow: Boolean,
    val followLimit: Int,
    val isFirstFollow: Boolean,
    val isLoop: Boolean,
    val timeBetweenLoop: Long,
    val isLikeLastPostBeforeFollow: Boolean,
    val isFollowToPrivate: Boolean,
    val likeCountLimit: Int,
    val passUsersWithLikeCountLimited: Boolean)

object ComplexBot1Descriptor {

  val LIKE_COUNT_LIMIT = "likeCountLimit"
  
  val PASS_USERS_WITH_LIKE_COUNT_LIMITED = "passUsersWithLikeCountLimited"
  
  val LESFI = "lesfi"
  
  val IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW = "isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow"
  
  val IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW = "isNeedToFillSelfFollowersAsNonToFollow"
  
  val FOLLOW_LIMIT = "followLimit" 
  
  val IS_FIRST_FOLLOW = "isFirstFollow"
  
  val IS_LOOP = "isLoop"
  
  val TIME_BETWEEN_LOOP = "timeBetweenLoop"
  
  val IS_LIKE_LASTPOST_BEFORE_FOLLOW = "isLikeLastPostBeforeFollow"
  
  val IS_FOLLOW_TO_PRIVATE = "isFollowToPrivate"
  
}
    
class ComplexBot1Descriptor extends IBEFEBotDescriptor("Complex robot v1") {

  override def createPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.complexBot1ConfigPage(ownerUser, apiId, configForm, this)

  override def infoPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: play.api.mvc.Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.complexBot1InfoPage(ownerUser, apiId, configForm, this)        
    
  override def configForm: Form[ComplexBot1FormConfig] = Form(
    mapping(
        ComplexBot1Descriptor.LESFI -> nonEmptyText,
        ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW -> boolean,
        ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW -> boolean, 
        ComplexBot1Descriptor.FOLLOW_LIMIT -> number(0, 7000),
        ComplexBot1Descriptor.IS_FIRST_FOLLOW -> boolean,
        ComplexBot1Descriptor.IS_LOOP -> boolean,
        ComplexBot1Descriptor.TIME_BETWEEN_LOOP -> longNumber(0, 2419200000L),
        ComplexBot1Descriptor.IS_LIKE_LASTPOST_BEFORE_FOLLOW -> boolean,
        ComplexBot1Descriptor.IS_FOLLOW_TO_PRIVATE -> boolean,
        ComplexBot1Descriptor.LIKE_COUNT_LIMIT -> number,
        ComplexBot1Descriptor.PASS_USERS_WITH_LIKE_COUNT_LIMITED -> boolean
    )(ComplexBot1FormConfig.apply)(ComplexBot1FormConfig.unapply)
    .verifying("Failed form constraints!", fields => 
      fields match {
        case data => new LESFICompiler().checks(data.lesfi)
      }))

  override def descr: String = name

  override def updateConfig(configData: Any, config: IBEFEBotConfig) =
    configData match {
      case c: ComplexBot1FormConfig =>
        config + (ComplexBot1Descriptor.LESFI, c.lesfi)
        config + (ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW, c.isNeedToFillSelfFollowersAsNonToUnfollowAndNonToFollow)
        config + (ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW, c.isNeedToFillSelfFollowingsAsNonToFollow)
        config + (ComplexBot1Descriptor.FOLLOW_LIMIT, c.followLimit)
        config + (ComplexBot1Descriptor.IS_FIRST_FOLLOW, c.isFirstFollow)
        config + (ComplexBot1Descriptor.IS_LOOP, c.isLoop)
        config + (ComplexBot1Descriptor.TIME_BETWEEN_LOOP, c.timeBetweenLoop)
        config + (ComplexBot1Descriptor.IS_LIKE_LASTPOST_BEFORE_FOLLOW, c.isLikeLastPostBeforeFollow)
        config + (ComplexBot1Descriptor.IS_FOLLOW_TO_PRIVATE, c.isFollowToPrivate)
        config + (ComplexBot1Descriptor.LIKE_COUNT_LIMIT, c.likeCountLimit)
        config + (ComplexBot1Descriptor.PASS_USERS_WITH_LIKE_COUNT_LIMITED, c.passUsersWithLikeCountLimited)
        true
      case _ => false
    }

  override def createHelper(config: IBEFEBotConfig): Option[IBEFEBotHelper] = {
    Some(new IBEFEBotHelper(LogStorageLogger(config get[LogStorage] LogStorage.LOG), config, name) {
      override def createBotProcess(config: IBEFEBotConfig, id: Long): Option[IBEFEBotProcess] = {
        val accountInfo = config get[AccountAPI] "account"
        val lesfi = config getString ComplexBot1Descriptor.LESFI
        val isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow = config getBoolean ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW 
        val isNeedToFillSelfFollowersAsNonToFollow = config getBoolean ComplexBot1Descriptor.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW
        val followLimit = config getInt ComplexBot1Descriptor.FOLLOW_LIMIT
        val isFirstFollow = config getBoolean ComplexBot1Descriptor.IS_FIRST_FOLLOW
        val isLoop = config getBoolean ComplexBot1Descriptor.IS_LOOP
        val timeBetweenLoop = config getLong ComplexBot1Descriptor.TIME_BETWEEN_LOOP
        val isLikeLastPostBeforeFollow = config getBoolean ComplexBot1Descriptor.IS_LIKE_LASTPOST_BEFORE_FOLLOW
        val isFollowToPrivate = config getBoolean ComplexBot1Descriptor.IS_FOLLOW_TO_PRIVATE
        val likeCountLimit = config getInt ComplexBot1Descriptor.LIKE_COUNT_LIMIT
        val passUsersWithLikeCountLimited = config getBoolean ComplexBot1Descriptor.PASS_USERS_WITH_LIKE_COUNT_LIMITED
        val api = new APIExecutor(logger, accountInfo.userName, accountInfo.instaLogin) 
        Some(new ComplexBot1(
            logger, 
            id, 
            name, 
            api, 
            lesfi, 
            isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow,
            isNeedToFillSelfFollowersAsNonToFollow,
            followLimit,
            isFirstFollow,
            isLoop,
            timeBetweenLoop,
            isLikeLastPostBeforeFollow,
            isFollowToPrivate,
            likeCountLimit,
            passUsersWithLikeCountLimited))
      }
    })
  }

}

object ComplexBot1 {
  
  val STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow = SPStates.STATE_NEEDS_FOLLOW + 1
  
  val STATE_fillSelfFollowersAsNonToFollow = STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow + 1
  
  val STATE_follow = STATE_fillSelfFollowersAsNonToFollow + 1
  
  val STATE_unfollow = STATE_follow + 1
  
  val STATE_MAIN_LOOP_PREPARE = STATE_unfollow + 1
  
  val STATE_Wait_loop = STATE_MAIN_LOOP_PREPARE + 1
  
  val STATE_SMALL_PAUSE_follow  = STATE_Wait_loop + 1
  
  val STATE_SMALL_PAUSE_unfollow  = STATE_SMALL_PAUSE_follow + 1
  
  val STATE_NEEDS_PREPARE  = STATE_SMALL_PAUSE_unfollow + 1
  
}

class ComplexBot1(
    logger: ds13.logger.Logger, 
    id: Long,  
    name: String, 
    api: APIExecutor, 
    lesfi: String,
    isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow: Boolean,
    isNeedToFillSelfFollowersAsNonToFollow: Boolean,
    followLimit: Int,
    isFirstFollow: Boolean,
    isLoop: Boolean,
    timeBetweenLoop: Long,
    isLikeLastPostBeforeFollow: Boolean,
    isFollowToPrivate: Boolean,
    likeCountLimit: Int,
    passUsersWithLikeCountLimited: Boolean) 
  extends InstBot(logger, id, name, api) {

  state = ComplexBot1.STATE_NEEDS_PREPARE

  val provider = new LESFICompiler().parseProvider(api, lesfi).get
  
  var summaryUsersFollowedCount = 0
  
  var summaryUsersUnfollowedCount = 0
  
  var userId: Long = -1
  
  var nextIdFollowers: Option[String] = None
  
  var nextIdFollowings: Option[String] = None
  
  var followedStageCount = 0
  
  var prevState = SPStates.STATE_UNKNOWN
  
  var notToFollowStorage: TraitNotToFollowListStorage = null
  
  var notToUnfollowStorage: TraitNotToUnFollowListStorage = null
  
  var notToFollow = ListBuffer[Long]()
  
  var unfollowListOpt: Option[UsersList] = None
  
  var unfollowListIndex = 0
  
  var toUnfollow = ListBuffer[User]()
  
  val filter = Some(new TraitFilter[User] {
    
    override def skip(user: User): Boolean =
      notToFollow.contains(user.id) || notToFollowStorage.contains(user.id)
    
  })
  
  override def catchedStep: Unit =
    state match {
      case ComplexBot1.STATE_NEEDS_PREPARE =>
        debug("Prepare in progress...")
        if (!api.executeInternal { internalAPI =>
          if(isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow) {
            setState(ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow)
          } else if(isNeedToFillSelfFollowersAsNonToFollow) {
            setState(ComplexBot1.STATE_fillSelfFollowersAsNonToFollow)
          } else {
            setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)
          }
          debug("State has been changed after prepare to " + state)
        }) errFinish("Some internal problems with API initialization!")
      case ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow =>
        debug("Fill self followers state...")
        api.executeInternal{ iapi =>
          val usersList = iapi.getSelfFollowings(nextIdFollowings)
          api.getAccountStorage.foreach { as =>
            val ids = usersList.users.map(_.id)
            debug("Prepare followers size " + ids.length)
            as.addEverFollowedList(ids)
            as.addNotToFollowList(ids)
            if(isNeedToFillSelfFollowersAsNonToFollow) as.addNotToUnFollowList(ids)
            usersList.nextMaxIdOpt.fold (
              if(isNeedToFillSelfFollowersAsNonToFollow) {
                setState(ComplexBot1.STATE_fillSelfFollowersAsNonToFollow)
              } else {
                setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)
              }
            )( t => nextIdFollowings = Some(t))
          }
        }
      case ComplexBot1.STATE_fillSelfFollowersAsNonToFollow =>
        debug("Fill self followings state...")
        api.executeInternal{ iapi =>
          val usersList = iapi.getSelfFollowers(nextIdFollowers)
          api.getAccountStorage.foreach { as =>
            val ids = usersList.users.map(_.id)
            debug("Prepare followings size " + ids.length)
            as.addNotToFollowList(ids)
            usersList.nextMaxIdOpt.fold(setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE))( t => nextIdFollowers = Some(t))
          }
        }
      case ComplexBot1.STATE_MAIN_LOOP_PREPARE =>
        debug("Main loop router started...")
        var isFirst = false
        if(prevState == SPStates.STATE_UNKNOWN) {
          isFirst = true
          debug("Previous loop state is unknown.")
          prevState = if(isFirstFollow) ComplexBot1.STATE_unfollow else ComplexBot1.STATE_follow
          debug("Set prev state to: " + prevState + " - " + (if(isFirstFollow) "unfollow" else "follow") + ".")
        }
        prevState match {
          case ComplexBot1.STATE_unfollow =>
            debug("Previous loop state unknown or unfollow")
            debug("Prepare to follow loop....")            
            api.getAccountStorage.foreach { as =>
              notToFollowStorage = as.getNotToFollowList.get
              notToFollow = ListBuffer[Long]()
              if(provider.hasNext()(filter)) {
                followedStageCount = 0
                setState(if(isFirst) ComplexBot1.STATE_follow else ComplexBot1.STATE_Wait_loop)
                prevState = ComplexBot1.STATE_follow
              } else okFinish("Provider empty - all finnished!")
            }            
          case ComplexBot1.STATE_follow =>
            debug("Previous loop state unknown or follow")
            debug("Prepare to unfollow loop....")
            api.getAccountStorage.foreach { as =>
              notToUnfollowStorage = as.getNotToUnFollowList.get
              setState(if(isFirst) ComplexBot1.STATE_unfollow else ComplexBot1.STATE_Wait_loop)
              prevState = ComplexBot1.STATE_unfollow
              updateUnfollowList(None)
            }
          case _ => 
            errFinish("Uknown loop state: " + prevState)
        } 
      case ComplexBot1.STATE_Wait_loop =>
        debug("Waiting between loop stages finnished")
        setState(prevState)
      case ComplexBot1.STATE_follow | ComplexBot1.STATE_SMALL_PAUSE_follow =>
        debug("Follow state works now.")
        if(followedStageCount < followLimit) {
          provider.fold {
            debug("Provider has no more users...")
            isLoopFinish
          } { user =>
            debug("User taken from provider: " + user.username + " with id " + user.id)
            if(notToFollow.contains(user.id) || notToFollowStorage.contains(user.id)) {
              debug("User can't be followed because in not followed list: " + user.username + " with id " + user.id)
              setState(ComplexBot1.STATE_SMALL_PAUSE_follow)
            } else if(user.isPrivate && !isFollowToPrivate) {
              debug("User can't be followed because not to follow private users option enabled: " + user.username + " with id " + user.id)
              setState(ComplexBot1.STATE_SMALL_PAUSE_follow)
            } else api.executeInternal { internalAPI =>
              var limitByLike = false
              if (isLikeLastPostBeforeFollow) {
                debug("It needs to like one post before follow... Try to get feed for " + user.id)
                if (user.isPrivate)
                  debug("Can't get feeds from private user. In this case I can't like his posts")
                else 
                  try {
                    val feed = internalAPI.getUserFeed(user.id)
                    debug("User have last " + feed.items.length + " feed items")
                    if (feed.items.length > 0) {
                      debug("Try to find post with minimum likes...")
                      val items = feed.items.sortBy(_.likeCount)
                      val item = items(0)
                      debug("Post have likes count: " + item.likeCount)
                      if(passUsersWithLikeCountLimited) {
                        if(item.likeCount < likeCountLimit) {
                          debug("Try to like post with id " + item.pk)
                          internalAPI.like(feed.items.last.pk)                          
                        } else {
                          debug("Post can't be liked because pass user with post likes limit options enabled and limit reached for user: " + 
                              user.username + " with id " + user.id + " - post " + item.id +  " with likes count " + item.likeCount + " and limit " + likeCountLimit)
                          setState(ComplexBot1.STATE_SMALL_PAUSE_follow)
                          limitByLike = true
                        }
                      } else {
                        debug("Try to like post with id " + item.pk)
                        internalAPI.like(feed.items.last.pk)
                      }
                    }
                  } catch {
                    case e: NotAuthorizedToViewUser =>
                      debug("NotAuthorizedToViewUser exception prevented for user " + user)
                  }
              }
              if(limitByLike) {
                debug("User can't be followed because option to pass limited by post likes not checked and limit has reached: " + user.username + " with id " + user.id)
                setState(ComplexBot1.STATE_SMALL_PAUSE_follow)
              } else {
                debug("Try to follow user: " + user.toString)
                val info = internalAPI.follow(user.id)
                api.getAccountStorage.foreach { as =>
                  as.addEverFollowedList(user.id)
                  as.addNotToFollowList(user.id)
                  notToFollow += user.id
                  followedStageCount += 1
                  summaryUsersFollowedCount += 1
                  toUnfollow += user
                  debug("User: " + user.username + " with id " + user.id + "  - followed successfully")
                  printStat
                  setState(ComplexBot1.STATE_follow)
                }
              }
            }
          }
        } else {
          debug("Follow limit per stage " + followedStageCount + " reached.")
          isLoopFinish
        }
     case ComplexBot1.STATE_unfollow | ComplexBot1.STATE_SMALL_PAUSE_unfollow =>
        debug("Unfollow state works now.")
        if(toUnfollow.isEmpty) {
          unfollowListOpt.fold(errFinish("Somthig went wrong, because unfollow list opt is null!")) { unfollowList =>
            while (
              if(unfollowListIndex >= unfollowList.users.length) {
                unfollowList.nextMaxIdOpt.fold {
                  isLoopFinish
                  false
                } { nextMaxId => 
                  updateUnfollowList(Some(nextMaxId))
                  unfollowListOpt match {
                    case Some(unfollowList) =>
                      if(unfollowList.users.length > 0) {
                        unfollowListOpt.foreach(unfollowUserFromUserList)
                        false
                      } else true
                    case _ => false
                  }
                }
              } else {
                unfollowUserFromUserList(unfollowList)
                false
              }
            ) { }
          }
        } else {
          val user = toUnfollow.head
          unfollowUser(user)
          toUnfollow -= user
        }
      case _ =>
        errFinish("Unknwon state occurs!")
    }

  def unfollowUserFromUserList(unfollowList: UsersList) = { 
    unfollowUser(unfollowList.users(unfollowListIndex))
    unfollowListIndex += 1
  }
  
  def unfollowUser(user: User) = {
    if(notToUnfollowStorage.contains(user.id)) {
      debug("User can't be unfollowed because in not unfollowed list: " + user.username + " with id " + user.id)
      setState(ComplexBot1.STATE_SMALL_PAUSE_unfollow)
    } else api.executeInternal { internalAPI =>
      debug("Try to unfollow user: " + user.toString)
      internalAPI.unfollow(user.id)
      api.getAccountStorage.foreach { as =>
        summaryUsersUnfollowedCount += 1
        debug("User: " + user.username + " with id " + user.id + "  - unfollowed successfully")
        printStat
        setState(ComplexBot1.STATE_unfollow)
      }
    }    
  }
  
  def printStat: Unit = {
    debug("Summay   followed: " + summaryUsersFollowedCount)
    debug("Summay unfollowed: " + summaryUsersUnfollowedCount)
  }
  
  def updateUnfollowList(nextMaxIdOpt: Option[String]) =
    api.executeInternal { iapi => 
      unfollowListOpt = Some(iapi.getSelfFollowings(nextMaxIdOpt))
      unfollowListIndex = 0
    }
  
  def isLoopFinish = 
    if(isLoop) setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE) else {
      printStat
      okFinish("Finished!")
    }
  
//  @tailrec
//  final def catchedRec(f: => Unit): Unit = 
//    try f catch {
//      case e: BadGatewayException =>
//        err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
//        e.dump(logger)
//        debug("Try again after 60000ms")
//        Thread.sleep(60000)
//        catchedRec(f)
//      case e: Throwable => throw e
//    }
//  
//  def catched(f: => Unit): Unit = 
//    try f catch {
//      case e: BadGatewayException => 
//        err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
//        e.dump(logger)
//        debug("Try again after 60000ms")
//      case e: Throwable => throw e
//    }

  override def getPause =
    state match {
      case ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow => 1000
      case ComplexBot1.STATE_fillSelfFollowersAsNonToFollow => 1000
      case ComplexBot1.STATE_MAIN_LOOP_PREPARE => 1000
      case ComplexBot1.STATE_SMALL_PAUSE_follow => 1000
      case ComplexBot1.STATE_unfollow => 40000
      case ComplexBot1.STATE_follow => 90000
      case ComplexBot1.STATE_SMALL_PAUSE_unfollow => 1000
      case ComplexBot1.STATE_Wait_loop => timeBetweenLoop
      case SPStates.STATE_NEEDS_FOLLOW => 90000
      case _                           => 1000
    }
  
  override def toJSON: Option[JSONObject] =
    Some(new JSONObject { 
      accumulate("provider", provider.toJSON.get)
    })
    
}

