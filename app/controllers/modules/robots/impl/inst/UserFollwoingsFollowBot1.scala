package controllers.modules.robots.impl.inst

import org.json.JSONObject

import controllers.modules.robots.IBEFEBotConfig
import controllers.modules.robots.IBEFEBotDescriptor
import controllers.modules.robots.IBEFEBotHelper
import controllers.modules.robots.IBEFEBotProcess
import controllers.modules.robots.LogStorage
import controllers.modules.robots.LogStorageLogger
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import ds13.insta.impl.rc1.APIExecutor
import models.AccountAPI
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.i18n.Messages
import play.api.mvc.Flash
import ds13.insta.impl.rc1.SPStates

case class UserFollowingsFollowBot1FormConfig(val username: String)

class UserFollowingsFollowBot1Descriptor extends IBEFEBotDescriptor("User followings follow robot v1") {

  val USERNAME = "username"

  override def createPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.userFollowingsFollowBot1ConfigPage(ownerUser, apiId, configForm, this)

  override def infoPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: play.api.mvc.Flash, messages: Messages): play.twirl.api.Html =
    views.html.admin.robots.confs.inst.userFollowingsFollowBot1InfoPage(ownerUser, apiId, configForm, this)        
    
  override def configForm: Form[UserFollowingsFollowBot1FormConfig] = Form(
    mapping(USERNAME -> nonEmptyText)(UserFollowingsFollowBot1FormConfig.apply)(UserFollowingsFollowBot1FormConfig.unapply))

  override def descr: String = name

  override def updateConfig(configData: Any, config: IBEFEBotConfig) =
    configData match {
      case c: UserFollowingsFollowBot1FormConfig =>
        config + (USERNAME, c.username)
        true
      case _ => false
    }

  override def createHelper(config: IBEFEBotConfig): Option[IBEFEBotHelper] = {
    Some(new IBEFEBotHelper(LogStorageLogger(config get[LogStorage] LogStorage.LOG), config, name) {
      override def createBotProcess(config: IBEFEBotConfig, id: Long): Option[IBEFEBotProcess] = {
        val accountInfo = config get[AccountAPI] "account"
        val usernameToGetFollowings = config getString USERNAME
        val api = new APIExecutor(logger, accountInfo.userName, accountInfo.instaLogin)
        Some(new UserFollowingsFollowBot1(logger, id, name, api, usernameToGetFollowings))
      }
    })
  }

}

class UserFollowingsFollowBot1(
    logger: ds13.logger.Logger, 
    id: Long,  
    name: String, 
    api: APIExecutor, 
    usernameToGetFollowings: String) 
  extends InstBot(logger, id, name, api) {

  state = SPStates.STATE_NEEDS_FOLLOW

  val provider = new UserFollowingsUsersProvider(api, usernameToGetFollowings)
  
  var preparedUsersCount = 0
  
  override def catchedStep: Unit =
    state match {
      case SPStates.STATE_NEEDS_FOLLOW =>
        provider.fold{
          debug("Provider has no more users!")
          debug("Summay followed: " + preparedUsersCount)
          setState(SPStates.STATE_FINISHED)
          isFinishedFlag = true          
        } { user =>
          api.executeInternal { internalAPI =>
            debug("Try to follow user with Id: " + user.toString)
            // TODO: Should add to DB
            internalAPI.follow(user.id)
            preparedUsersCount += 1
            debug("User: " + user.toString + " - followed successfully")
            debug("Summay followed: " + preparedUsersCount)
          }
        }
      case _ =>
        err("Unknwon state occurs!")
        setState(SPStates.STATE_FINISHED)
        isFinishedFlag = true
    }

  override def getPause =
    state match {
      case SPStates.STATE_NEEDS_FOLLOW => 90000
      case _                           => 1000
    }
  
  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("provider", provider.toJSON.get)
    })


}

