package controllers.modules.robots

import ds13.insta.api.intrnl.model.providers.users.TraitUsersProvider
import ds13.insta.impl.rc1.APIExecutor

trait ProviderFactory {

  def create(api: APIExecutor): TraitUsersProvider

}