package controllers.modules.robots

import java.io.File
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException

import javax.inject.Singleton
import play.Logger
import java.io.PrintWriter

object LogStorage {

  val LOG = "log"

}

class LogStorage(path: String) {

  var logLinesCount = 0

  val logLinesLimit = 10000

  var fileWriterOpt: Option[FileWriter] = None

  var index = 0

  private def createFileWriter =
    fileWriterOpt =
      try {
        val file = new File(path)
        if (!file.exists)
          file.createNewFile
        val fileWriter = new FileWriter(file)
        Some(fileWriter)
      } catch {
        case e: IOException =>
          Logger.error("Couldn't create file writer: Couldn't get path for " + path, e)
          None
        case e: FileNotFoundException =>
          Logger.error("Couldn't create file writer: Couldn't get path for " + path + " - file not found exception", e)
          None
      }

  def rebase =
    if (logLinesCount >= logLinesLimit) {
      fileWriterOpt foreach { fileWriter =>
        fileWriter.flush
        fileWriter.close
        new File(path).renameTo(new File(path + "-old-" + index))
        index += 1
        createFileWriter
      }
      logLinesCount = 0
    }

  def append(msg: String) = {
    if (fileWriterOpt.isEmpty) createFileWriter else rebase
    fileWriterOpt.foreach { fileWriter =>
      fileWriter.append(msg + "\n")
      fileWriter.flush
      logLinesCount += 1
    }
  }

}