package controllers.modules.robots

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter

import org.json.JSONObject

import ds13.bots.AbstractBotDescriptor
import ds13.bots.AbstractBotHelper
import ds13.bots.BotController
import ds13.bots.ControlMessage
import ds13.bots.Remove
import ds13.bots.StateMachineBotProcess
import ds13.bots.TraitBotConfig
import ds13.logger.LoggerSupported
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import play.api.data.Form
import play.api.i18n.Messages
import ds13.insta.bots.InstaBotHelper
import ds13.insta.bots.InstaBotConfig
import ds13.insta.bots.InstaBotDescriptor
import ds13.insta.bots.InstaBotResult
import ds13.insta.bots.InstaBotProcessConfig
import ds13.insta.bots.InstaBotProcess
import ds13.insta.bots.InstaBotController
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.impl.rc1.SPStates
import java.io.StringWriter
import java.io.PrintWriter
import ds13.insta.api.DS13Exception
import ds13.insta.api.DS13Exception
import ds13.insta.api.ClientException

abstract class IBEFEBotHelper(logger: ds13.logger.Logger, config: IBEFEBotConfig, val descriptorName: String)
    extends InstaBotHelper[IBEFEBotResult, IBEFEBotProcessConfig, IBEFEBotConfig, IBEFEBotProcess](logger, config) {

  var uid: Long = -1

  override def createActor(uid: Long) = {
    this.uid = uid
    super.createActor(uid)
  }

}

class IBEFEBotConfig(val props: (String, Any)*) extends InstaBotConfig {

  var propsMap = props.toMap

  def +(key: String, value: Any) = {
    propsMap += (key -> value)
    this
  }

  def getString(key: String): String = get[String](key).toString

  def getInt(key: String): Int = get[Int](key)

  def getLong(key: String): Long = get[Long](key)

  def getBoolean(key: String): Boolean = get[Boolean](key)

  def get[T](key: String): T = propsMap(key).asInstanceOf[T]

}

abstract class IBEFEBotDescriptor(name: String)
    extends InstaBotDescriptor[IBEFEBotResult, IBEFEBotProcessConfig, IBEFEBotConfig, IBEFEBotProcess, IBEFEBotHelper](name) {

  override def newConfig: IBEFEBotConfig = new IBEFEBotConfig

  def updateConfig(configData: Any, config: IBEFEBotConfig): Boolean

  def createPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: play.api.mvc.Flash, messages: Messages): play.twirl.api.Html

  def infoPage(ownerUser: models.User, apiId: Long)(implicit user: models.User, flash: play.api.mvc.Flash, messages: Messages): play.twirl.api.Html

  def descr: String = ""

  def configForm: Form[_]

}

class IBEFEBotResult extends InstaBotResult

class IBEFEBotProcessConfig extends InstaBotProcessConfig

class IBEFEBotProcess(logger: ds13.logger.Logger, id: Long, api: APIExecutor, name: String)
    extends InstaBotProcess[IBEFEBotResult, IBEFEBotProcessConfig](logger, id, api, name) {

  override def getPause: Long = 1000

  override def toJSON: Option[JSONObject] = None

  override def step: Unit =
    try catchedStep catch { case e: Throwable => catchExceptions(e) }

  def catchExceptions(e: Throwable): Unit = {
    err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
    e match {
      case ed: ClientException => ed.dump(logger)
      case _ =>
    }
    throw e
  }

  def catchedStep: Unit = {}

  override def setState(s: Int) {
    super.setState(s)
    debug("State changed to: " + s)
  }

  def errFinish(msg: String) {
    err(msg)
    setState(SPStates.STATE_FINISHED)
    isFinishedFlag = true
  }

  def okFinish(msg: String) {
    debug(msg)
    setState(SPStates.STATE_FINISHED)
    isFinishedFlag = true
  }

}

@Singleton
class IBEFEBotContoller @Inject() (dao: DAO, robotsProvider: RobotsProvider)
    extends InstaBotController[IBEFEBotResult, IBEFEBotProcessConfig, IBEFEBotConfig, IBEFEBotProcess, IBEFEBotHelper, IBEFEBotDescriptor] {

  robotsProvider.descriptors foreach addDescriptor

  val accountIdToIds = new java.util.concurrent.ConcurrentHashMap[Long, List[Long]]()

  def createConfiguredBot(accountId: Long, bid: Long, descriptorName: String)(f: IBEFEBotConfig => Boolean): Option[Long] = {
    super.createConfiguredBot(descriptorName, bid: Long)(f) map { id =>
      if (accountIdToIds containsKey accountId)
        accountIdToIds.put(accountId, accountIdToIds.get(accountId).+:(id))
      else {
        accountIdToIds.put(accountId, List(id))
      }
      id
    }
  }

  def getByAccountAPIID(id: Long): Seq[IBEFEBotHelper] =
    if (accountIdToIds containsKey id)
      accountIdToIds.get(id).map(bid => getBotHelper(bid).get)
    else Seq.empty

  def getByAccountAPIIDWithDescr(id: Long): Seq[(IBEFEBotHelper, IBEFEBotDescriptor)] = {
    if (accountIdToIds containsKey id) {
      accountIdToIds.get(id).map(bid => getBotHelper(bid).get) map (h => (h, getDescriptor(h.descriptorName).get))
    } else Seq.empty
  }

  override def sendMsg(botId: Long, msg: ControlMessage): Boolean =
    msg match {
      case Remove() =>
        if (super.sendMsg(botId, msg)) {
          accountIdToIds.asScala.find { case (k, v) => v contains botId } foreach {
            case (k, v) =>
              accountIdToIds.put(k, v.filterNot(_ == botId))
          }
          true
        } else false
      case otherMsg => super.sendMsg(botId, msg)
    }

}