package controllers

import javax.inject.Inject
import javax.inject.Singleton
import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.daos.DAO

@Singleton
class AppController @Inject() (val dao: DAO) extends TraitUserServiceWithRoles with OptionalAuthElement {

}
