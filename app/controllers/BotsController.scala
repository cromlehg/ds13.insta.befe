package controllers

import ds13.bots.CommmonImplicits.appContext

import controllers.modules.robots.IBEFEBotContoller
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import play.Configuration
import play.api.Logger
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import play.api.mvc.Flash
import controllers.modules.robots.LogStorage
import scala.io.Source
import scala.concurrent.Future
import java.io.File
import play.api.mvc.AnyContent
import controllers.modules.robots.IBEFEBotDescriptor
import play.api.mvc.Action
import play.api.mvc.Request

@Singleton
class BotsController @Inject() (override val dao: DAO, val bc: IBEFEBotContoller, val messagesApi: MessagesApi, val config: Configuration) extends TraitDS13UserServiceWithRoles with I18nSupport {

  val storagePath = config.getString("ds13.bots.storage.logs") + "/"

  def botStoragePath(bid: Long) = storagePath + bid

  // TODO: Rewrite with client based checks and access
  def infoBot(launchId: Long) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      Future.successful(Ok("TODO"))
  }

  def botCreate(userId: Long, accountId: Long, name: String) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      // Should check for account ID owner access - if ADMIN or Owner!!!!
      dao.findUserById(userId) flatMap {
        case Some(ownerUser) =>
          dao.getAccountMainAPI(accountId) map {
            case Some(amapi) =>
              bc.getDescriptor(name) match {
                case Some(descr) => Ok(descr.createPage(ownerUser, amapi.id))
                case _ => BadRequest("Descriptor with name " + name + " not found!")
              }
            case _ => BadRequest("Account with id " + accountId + " not found")
          }
        case _ => Future.successful(BadRequest("Пользователь с таким id = " + userId + " не найден"))
      }
  }

  //  def botCreate1(apiId: Long, name: String) = apiOwnerOrAdmin(apiId) {
  //    (apiId: Long, _, name: String, version: String, deviceId: Option[String], guid: Option[String], proxyId: Option[Long]) =>
  //      (accountId: Long, userId: Long, login: String, pass: String, instaAccountId: Option[Long]) =>
  //        ownerUser: User =>
  //          implicit user =>
  //            implicit request =>
  //              Future.successful(bc.getDescriptor(name) match {
  //                case Some(descr) => Ok(descr.createPage(ownerUser, apiId))
  //                case _           => BadRequest("Descriptor with name " + name + " not found!")
  //              })
  //  }

  def botAction(launchId: Long, actionName: String) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      Logger.debug("botAction(" + launchId + ", \"" + actionName + "\")")
      dao.findLaunchById(launchId) flatMap {
        case Some(launch) =>
          if (bc.sendMsg(launchId, actionName)) {
            if (actionName equalsIgnoreCase "remove") {
              Logger.debug("Try to remove launch with id " + launchId + " from DB.")
              dao.deleteLaunch(launchId) map {
                case true =>
                  Logger.debug("Launch with id " + launchId + " successfully removed from DB.")
                  val path = botStoragePath(launchId)
                  Logger.debug("Checks if exists log file \"" + path + "\" for lanuch with id " + launchId)
                  new File(storagePath).listFiles.filter(t => t.exists && t.isFile && t.getName.startsWith(launchId + "-old-")).foreach(_.delete)
                  val file = new File(path)
                  if (file.exists) {
                    Logger.debug("Log file exists. Try to remove log file \"" + path + "\" for lanuch with id " + launchId)
                    try {
                      file.delete
                      Logger.debug("Log file \"" + path + "\" for lanuch with id " + launchId + " successfully removed!")

                      val fileState = new File(path + "-state")
                      if (fileState.exists) {
                        Logger.debug("State file exists. Try to remove log file \"" + path + "-state\" for lanuch with id " + launchId)
                        try {
                          fileState.delete
                          Logger.debug("State file \"" + path + "-state\" for lanuch with id " + launchId + " successfully removed!")
                          Redirect(request headers "referer")
                            .flashing("success" -> ("Command " + actionName + " successuflly sended to robot launch " + launchId))
                        } catch {
                          case e: Throwable =>
                            Logger.error("Failed to remove state file \"" + path + "-state\" for lanuch with id " + launchId + "!", e)
                            Redirect(request headers "referer")
                              .flashing("success" -> ("All successfull except error during removing state file \"" + path + "-state\" for lanuch with id " + launchId + "!"))
                        }
                      } else
                        Redirect(request headers "referer")
                          .flashing("success" -> ("Command " + actionName + " successuflly sended to robot launch " + launchId))

                    } catch {
                      case e: Throwable =>
                        Logger.error("Failed to remove log file \"" + path + "\" for lanuch with id " + launchId + "!", e)
                        Redirect(request headers "referer")
                          .flashing("success" -> ("All successfull except error during removing log file \"" + path + "\" for lanuch with id " + launchId + "!"))
                    }
                  } else
                    Redirect(request headers "referer")
                      .flashing("success" -> ("Command " + actionName + " successuflly sended to robot launch " + launchId))
                case _ =>
                  Redirect(request headers "referer").flashing("error" -> ("Can't remove launch " + launchId + " from db."))
              }
            } else {
              Future(Redirect(request headers "referer")
                .flashing("success" -> ("Command " + actionName + " successuflly sended to robot launch " + launchId)))
            }
          } else {
            Future(Redirect(request headers "referer")
              .flashing("error" -> ("Error occurs during command " + actionName + " send to robot launch " + launchId)))
          }
        case _ =>
          Future(BadRequest("Launch with this id " + launchId + " not found in db!"))
      }
  }

  def readBotLog(launchId: Long) = actionAdminAsyncRequestUser { implicit request =>
    implicit user =>
      Future.successful(Ok(views.html.admin.robots.robotLog {
        if (new File(botStoragePath(launchId)).exists)
          Source.fromFile(botStoragePath(launchId), "UTF-8").getLines.mkString("\n")
        else
          ""
      }))
  }

  def botSave(userId: Long, accountId: Long, name: String) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      dao.getAccountMainAPI(accountId) flatMap {
        case Some(amapi) =>
          bc.getDescriptor(name) match {
            case Some(descr) =>
              val newConfigForm = descr.configForm.bindFromRequest
              Logger.debug("Form content for botSave: ")
              Logger.debug(newConfigForm.toString)
              newConfigForm.fold(
                hasErrors = { form =>
                  Future.successful(Redirect(controllers.routes.BotsController.botCreate(userId, accountId, name)).
                    flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы")))
                },
                success = { data =>
                  dao.createLaunch(name, accountId, -1) map (_ match {
                    case Some(launch) =>
                      val uid = launch._1
                      val logStorage = new LogStorage(botStoragePath(uid))
                      logStorage.append("Start to create robot " + uid + " with name: " + name + " for account with id " + accountId + " for user with id " + userId)
                      bc.createConfiguredBot(accountId, uid, name)(cfg => descr.updateConfig(data,
                        cfg +
                          ("inst", amapi) +
                          (LogStorage.LOG, logStorage) +
                          ("account", amapi))) match {
                        case Some(botId) =>
                          Logger.debug("Bot \"" + name + "\" with id: " + botId + " for user with id " + userId + " successfully created!")
                          Logger.debug("Bot count: " + bc.botsCount())
                          logStorage.append("Robot with name: " + name + " for account with id " + accountId + " for user with id " + userId + " successfully created!")
                          Redirect(controllers.routes.AccountsController.account(userId, accountId)).
                            flashing("success" -> ("Robot successfully created from description \"" + descr.name + "\""))
                        case _ => BadRequest("Some problems during robot creation " + name + "!")
                      }
                    case _ =>
                      BadRequest("Couldn't create launch record for bot " + name + " with accountID + " + accountId + " for user" + userId)
                  })
                })
            case _ => Future.successful(BadRequest("Descriptro with name " + name + " not found!"))
          }
        case _ => Future.successful(BadRequest("Пользователь с таким id = " + userId + " не найден"))
      }
  }

}
