package controllers

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import controllers.modules.robots.IBEFEBotContoller
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO

@Singleton
class AccountsController @Inject() (override val dao: DAO, val bc: IBEFEBotContoller) extends TraitUserServiceWithRoles {

  def list(pageId: Long) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      dao.getAccountsPage(pageId).flatMap {
        accounts => Future(Ok(views.html.admin.accounts.list(accounts)))
      }
  }

  def account(userId: Long, accountId: Long) = actionAdminAsyncRequestUser { implicit user =>
    implicit request =>
      dao.findUserById(userId) flatMap {
        case Some(ownerUser) =>
          dao.getAccountMainAPI(accountId) map {
            case Some(amapi) => Ok(views.html.admin.accounts.accountProfile(ownerUser, amapi, bc.descrsPage(100, 1), bc.getByAccountAPIIDWithDescr(amapi.id)))
            case _           => BadRequest("Account with id = " + accountId + " not exists")
          }
        case _ => Future.successful(BadRequest("Пользователь с таким id = " + userId + " не найден"))
      }
  }

}
