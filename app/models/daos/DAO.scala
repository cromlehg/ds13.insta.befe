package models.daos

import scala.concurrent.Future

import javax.inject.Inject
import models.AccountStatus
import models.Param
import models.Timezone
import models.User
import models.UserStatus
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import controllers.MediaUtils
import models.UserStatus
import slick.driver.MySQLDriver
import ds13.logger.DummyLogger
import slick.driver.JdbcProfile
import models.AccountAPI
import models.SimpleProxyProperties
import ds13.http.TraitProxyProperties
import java.sql.Blob

/**
 *
 * Queries with SlickBUG should be replace leftJoin with for comprehesive. Bug:
 * "Unreachable reference to after resolving monadic joins"
 *
 */

// inject this 
// conf: play.api.Configuration, 
// and then get conf value
// conf.underlying.getString(Utils.meidaPath)
class DAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, mediaUtils: MediaUtils) extends DAOSlick with TraitSolved {

  import driver.api._

  val pageSize = 10

  val timezones = Map(1 -> "Europe/Moscow")

  val accountStatuses = Map(1 -> models.AccountStatus.ACTIVE, 2 -> models.AccountStatus.LOCKED)

  val userStatuses = Map(1 -> models.UserStatus.ONLINE, 2 -> models.UserStatus.OFFLINE)

  def param(name: String) =
    db.run(params.filter(_.name === name).result.headOption).map(_.map(p => new Param(p.id, p.name, p.value)))

  def getAccountMainAPI(accountId: Long) = {
    val query = for {
      dbAccount <- accounts.filter(_.id === accountId)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAPI <- api.filter(_.accountId === dbAccount.id)
      dbProxy <- proxies.filter(_.id === dbAPI.proxyId)
    } yield (dbAccount, dbUser, dbAPI, dbProxy)
    db.run(query.result.headOption).map(_.map {
      case (dbAccount, dbUser, dbAPI, dbProxy) =>
        new AccountAPI(
          dbAPI._1,
          dbUser.login,
          dbAPI._2,
          dbAPI._3,
          dbAPI._4,
          dbAPI._5,
          dbAPI._6,
          dbAPI._7.map(t =>
            new SimpleProxyProperties(
              dbProxy._1,
              dbProxy._2,
              dbProxy._3,
              dbProxy._4,
              dbProxy._5)),
          dbAccount._5,
          dbAccount._3,
          dbAccount._4)
    })
  }

  def getAccountsPage(pageId: Long): Future[Seq[models.Account]] = {
    val query = for {
      dbAccount <- accounts.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result).map(_.map {
      case (dbAccount, dbUser) =>
        new models.Account(
          dbAccount._1,
          dbUser.id,
          dbUser.login,
          dbUser.email,
          dbUser.name,
          dbUser.surname,
          dbAccount._3,
          dbAccount._4,
          dbAccount._5)
    })
  }

  def getUsersPage(pageId: Long): Future[Seq[models.User]] =
    db.run(users.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result) map (_ map userFrom)

  def findMediaByIdWthoutOwner(id: Long) =
    db.run(media.filter(_.id === id).result.headOption).map(_.map { m =>
      new models.Media(mediaUtils.getFilesFolderAbs, m.id, None, m.path, m.mimeType, m.created)
    })

  def findUserById(id: Long) =
    getUserFromQuery(users.filter(_.id === id))

  def findUserByEmail(email: String): Future[Option[User]] =
    getUserFromQuery(users.filter(_.email === email))

  def findUserBySUIDAndSessionId(sessionId: Long, suid: String): Future[Option[User]] = {
    val query = for {
      dbSession <- sessions.filter(t => t.id === sessionId && t.suid === suid)
      dbUser <- users.filter(_.id === dbSession.userId)
    } yield (dbUser, dbSession)
    db.run(query.result.headOption).map(_.map {
      case (dbUser, dbSession) =>
        new User(
          dbUser.id,
          dbUser.email,
          dbUser.hash,
          Some(new models.Session(dbSession.id, dbSession.userId, dbSession.suid)),
          List(),
          None,
          new UserStatus(dbUser.userStatusId, userStatuses(dbUser.userStatusId)),
          new AccountStatus(dbUser.accountStatusId, accountStatuses(dbUser.accountStatusId)),
          dbUser.name,
          dbUser.surname,
          new Timezone(dbUser.timezoeId, timezones(dbUser.timezoeId)),
          dbUser.registered)
    })
  }

  def getUserFromQuery(query: Query[(Users), (DBUser), Seq]) =
    db.run(query.result.headOption).map(_.map(userFrom))

  def userFrom(dbUser: DBUser) =
    new User(
      dbUser.id,
      dbUser.email,
      dbUser.hash,
      None,
      List(),
      None,
      new UserStatus(dbUser.userStatusId, userStatuses(dbUser.userStatusId)),
      new AccountStatus(dbUser.accountStatusId, accountStatuses(dbUser.accountStatusId)),
      dbUser.name,
      dbUser.surname,
      new Timezone(dbUser.timezoeId, timezones(dbUser.timezoeId)),
      dbUser.registered)

  def findUserWithRolesById(userId: Long): Future[Option[User]] =
    updateUserWithRoles(findUserById(userId))

  def updateUserWithRoles(futureOptUser: Future[Option[User]]): Future[Option[User]] =
    futureOptUser flatMap {
      case Some(u) =>
        findRolesByUserId(u.id).map { r =>
          Some(
            new User(
              u.id,
              u.email,
              u.hash,
              u.session,
              r.toList,
              None,
              u.userStatus,
              u.accountStatus,
              u.name,
              u.surname,
              u.timezone,
              u.registered))

        }
      case None => Future(None)
    }

  def findRolesByUserId(userId: Long) =
    db.run(userRoles.filter(_.userId === userId).result).map(_.map(_.role))

  def insertMedia(m: models.Media): Future[Option[models.Media]] = {
    val query = for {
      dbMedia <- (media returning media.map(_.id) into ((v, id) => v.copy(id = id))) +=
        new DBMedia(
          m.id,
          m.owner.flatMap { t => Some(t.id) },
          m.path,
          m.mimeType,
          m.created)
    } yield dbMedia
    db.run(query.transactionally) map (s =>
      Some(new models.Media(mediaUtils.getFilesFolderAbs, s.id, None, s.path, s.mimeType, s.created)))
  }

  def findAPIById(apiId: Long): Future[Option[(Long, Long, String, String, Option[String], Option[String], Option[Long])]] =
    db.run(api.filter(_.id === apiId).result.headOption)

  def findAccountById(accountId: Long): Future[Option[(Long, Long, String, String, Option[Long])]] =
    db.run(accounts.filter(_.id === accountId).result.headOption)

  def findLaunchById(launchId: Long): Future[Option[(Long, String, Long, Int)]] =
    db.run(launches.filter(_.id === launchId).result.headOption)

  def deleteLaunch(launchId: Long): Future[Boolean] =
    db.run(launches.filter(_.id === launchId).delete.transactionally) map { _ == 1 }

  def createLaunch(botName: String, apiId: Long, state: Int): Future[Option[(Long, String, Long, Int)]] = {
    val query = for {
      dbLaunch <- (launches returning launches.map(_.id) into ((v, id) => v.copy(_1 = id))) += (0, botName, apiId, state)
    } yield dbLaunch
    db.run(query.transactionally) map (s => Some((s._1, s._2, s._3, s._4)))
  }

  def updateLaunchState(launchId: Long, state: Int): Future[Boolean] =
    db.run(launches.filter(_.id === launchId).map(t => (t.state)).update(state).transactionally) map (r => if (r == 1) true else false)

  ////////////// HELPERS ////////////////

  @inline final def someToSomeFlatMap[T1, T2](f1: Future[Option[T1]], f2: T1 => Future[Option[T2]]): Future[Option[T2]] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(None)
    })

  @inline final def someToSomeFlatMapElse[T](f1: Future[Option[_]], f2: Future[Option[T]]): Future[Option[T]] =
    f1 flatMap (_ match {
      case Some(r) => Future.successful(None)
      case None    => f2
    })

  @inline final def someToBooleanFlatMap[T](f1: Future[Option[T]], f2: T => Future[Boolean]): Future[Boolean] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(false)
    })

  @inline final def someToSeqFlatMap[T1, T2](f1: Future[Option[T1]], f2: T1 => Future[Seq[T2]]): Future[Seq[T2]] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(Seq.empty[T2])
    })

  @inline final def seqToSeqFlatMap[T1, T2](f1: Future[Seq[T1]], f2: T1 => Future[T2]): Future[Seq[T2]] =
    f1 flatMap { rs =>
      Future.sequence {
        rs map { r =>
          f2(r)
        }
      }
    }

}
