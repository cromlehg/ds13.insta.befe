package models.daos

case class DBMedia(
  id: Long,
  ownerId: Option[Long],
  path: String,
  mimeType: Option[String],
  created: Long)

case class DBUser(
  id: Long,
  login: String,
  email: String,
  hash: Option[String],
  avatarId: Option[Long],
  userStatusId: Int,
  accountStatusId: Int,
  name: Option[String],
  surname: Option[String],
  timezoeId: Int,
  registered: Long)

case class DBSession(
  id: Long,
  userId: Long,
  suid: String)

case class DBUserRole(
  userId: Long,
  role: String)

case class DBParam(
  id: Long,
  name: String,
  value: String)

case class DBTimezone(
  id: Long,
  timezone: String)


