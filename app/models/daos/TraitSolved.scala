package models.daos

import java.util.concurrent.TimeoutException

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

import play.Logger

trait TraitSolved {

  val solverTimeoutInSec = 100

  def solved(f: Future[Boolean]): Boolean =
    Await.result(f, solverTimeoutInSec.second)

  def solved(f: Future[Int]): Int =
    Await.result(f, solverTimeoutInSec.second)

  def solved(f: Future[Long]): Long =
    Await.result(f, solverTimeoutInSec.second)

  def solved[T](f: Future[Seq[T]]): Seq[T] =
    Await.result(f, solverTimeoutInSec.second)

  def solved[T](f: Future[Option[T]]): Option[T] =
    Await.result(f, solverTimeoutInSec.second)
}